# README #
Simply include the action.h file in the driver program. Compile with the ``` #!c -l wiringPi  ``` option.

**Example:**

```
#!c

gcc -o output.exe *.c  -l wiringPi

sudo ./ouput.exe

```