#ifndef _4KBOTS_ACTION_
#define _4KBOTS_ACTION_
#include "acodes.h"
#include <stdio.h>
#include <wiringPi.h>
#include <sys/time.h>
#include <stdlib.h>
#include <time.h>

const int OUT_PUT_PIN = 17;
const int MAX_VALUE = 999999;
char  _4KBOT_INIT_ = 0;

void delayUnits(int);
void delayUsec(int);
void writeComand(int);
void init();


void delayUnits(int units)
{
	delayUsec( 800 * units);
}

void delayUsec(int delayTime)
{
	if(delayTime > MAX_VALUE){
		printf("Error delay time too large\n");
		return;
	}
	struct timeval curTime;	
	gettimeofday(&curTime,NULL);
	int endingTime=0;
	if((delayTime+curTime.tv_usec) > MAX_VALUE){
		//when microsecond returned is larger and needs to get to next second slot
		endingTime=delayTime-(MAX_VALUE-curTime.tv_usec)-1;
		while(endingTime<=curTime.tv_usec){
			gettimeofday(&curTime,NULL);
		}
	}else{
		endingTime=delayTime+curTime.tv_usec;
	}
	while(endingTime>=curTime.tv_usec){
		gettimeofday(&curTime,NULL);
	}
}

void writeComand(int command)
{
	if(_4KBOT_INIT_ == 0) {init();}
	digitalWrite(OUT_PUT_PIN,LOW);
	delayUnits(8);
	int unitsToJump=1;
	for(int i=0;i<8;i++){
		digitalWrite(OUT_PUT_PIN,HIGH);
		if ((command & 128)!=0){
			unitsToJump=4;
		}else {
			unitsToJump=1;
		}
		delayUnits(unitsToJump);
		digitalWrite(OUT_PUT_PIN,LOW);
		delayUnits(1);
		command<<=1;	
	}
	digitalWrite(OUT_PUT_PIN,HIGH);
}

void init(){
	if(_4KBOT_INIT_ != 0) {return;}
	wiringPiSetupGpio();
	pinMode(OUT_PUT_PIN,OUTPUT);
	digitalWrite(OUT_PUT_PIN,HIGH);
	_4KBOT_INIT_ = 1;	// Initialization flag 
}

#endif