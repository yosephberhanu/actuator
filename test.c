#include "action.h"
#include <time.h>

void waitFor (unsigned int secs) {
    unsigned int retTime = time(0) + secs;   // Get finishing time.
    while (time(0) < retTime);               // Loop until it arrives.
}

int main(){
	//init();
	int command = WALK_FWD;
	writeComand(command);
	waitFor(2);
	command = STOP;
	writeComand(command);
}